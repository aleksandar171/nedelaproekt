<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once 'vendor/autoload.php';
require_once 'vendor/fzaninotto/faker/src/autoload.php';

use Carbon\Carbon;

$faker = Faker\Factory::create();
$firstname = $faker->firstname();
$lastname = $faker->lastname();
$day = $faker->dayOfMonth();
$month = $faker->month();
$year = $faker->year();

$dateofbirth = Carbon::createFromDate($year, $month, $day);
$finalDate = $dateofbirth->toDateString();

//var_dump($);

$dsn = 'mysql:host=localhost:3306;dbname=facker;charset=utf8';
$usr = 'root';
$pwd = 'root';

$pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);

$insertStatement = $pdo->insert(array('firstname', 'lastname', 'dateofbirth'))
   ->into('users')
   ->values(array($firstname, $lastname, $finalDate));

$insertId = $insertStatement->execute(false);




